#include "tests.h"
#include <Wire.h>
#include "stm32lib.h"
//include "ExpanderMCP.h"

TestClass::TestClass() : mcp(0x20)
{
    foundDisplay = false;
    foundBME = false;
    foundMCP = false;
    foundSTM = false;
};

void TestClass::scani2c()
{
    byte error, address;
    int nDevices;

    Serial.println("\nScanning I2C ...");

    nDevices = 0;
    for (address = 1; address < 127; address++)
    {
        // The i2c_scanner uses the return value of
        // the Write.endTransmisstion to see if
        // a device did acknowledge to the address.
        Wire.beginTransmission(address);
        error = Wire.endTransmission();

        if (error == 0)
        {
            Serial.print("I2C device found at address 0x");
            if (address < 16)
                Serial.print("0");
            Serial.print(address, HEX);
            Serial.println("  !");

            nDevices++;
            if (address == BME_ADDRESS) {
                foundBME = true;
                Serial.println("Detected BME");
            }
            if (address == SSD1306_ADDR) {
                foundDisplay = true;
                Serial.println("Detected Display");
            }
            if (address == MCP_ADDRESS) {
                foundMCP = true;
                Serial.println("Detected MCP");
            }
            if (address == STM32_SLAVE_ADDR) {
                foundSTM = true;
                Serial.println("Detected STM32");
            }
        }
        else if (error == 4)
        {
            Serial.print("Unknown error at address 0x");
            if (address < 16)
                Serial.print("0");
            Serial.println(address, HEX);
        }
    }
    if (nDevices == 0)
        Serial.println("No I2C devices found");
    else
        Serial.println("I2C scan done");
}

bool TestClass::pingSTM32(){
    Serial.println("Trying to PING STM32");
    STM32 stm32;
    bool result = stm32.ping();
    foundSTM = true;
    if (result){
        Serial.print("STM32-Ping returned true\n");
    } else {
        Serial.print("STM32-Ping returned false\n");
    }
    return result;
}

void TestClass::testMCP(){
    byte data = 0x00;
    mcp.readPort(PortExpanderMCP::MCP_PORT_A, &data);
    Serial.print("Test: MCP PortA ");
    Serial.print(data, BIN);
    Serial.print("\n");

    mcp.setIODirection(PortExpanderMCP::MCP_PORT_A, 0x00); // Output = 0
}

void TestClass::setPortA(){
    Serial.print("TestClass: Setting PortA\n");
    mcp.writePort(PortExpanderMCP::MCP_PORT_A, 0x00);
    //mcp.latchPort(PortExpanderMCP::MCP_PORT_A);
}

void TestClass::unsetPortA(){
    Serial.print("TestClass: Unsetting PortA\n");
    mcp.writePort(PortExpanderMCP::MCP_PORT_A, 0xFF);
    //mcp.latchPort(PortExpanderMCP::MCP_PORT_A);
}

bool TestClass::isDisplayPresent(){
    return foundDisplay;
}

bool TestClass::isBMEPresent(){
    return foundBME;
}

bool TestClass::isMCPPresent(){
    return foundMCP;
}

bool TestClass::isSTMPresent(){
    return foundSTM;
}