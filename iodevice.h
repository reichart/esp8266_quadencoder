#ifndef _IODEVICE_H_
#define _IODEVICE_H_

/*
#if ARDUINO < 100
  #include <WProgram.h>
#else
  #include <Arduino.h>
#endif
*/

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class IODevice {
    public:
        virtual void init() = 0;
        virtual uint8_t getOutputs() = 0;
        virtual uint8_t getInputs() = 0;
        virtual bool getInput(uint8_t channel) = 0;
        virtual bool setOutputs(uint8_t channels) = 0;
        virtual bool setOutput(uint8_t channel, bool isHigh) = 0;
        virtual bool toggleOutput (uint8_t channel) = 0;

        bool setPullups(uint8_t isUp);
        bool setPullup(uint8_t channel, bool isUp);

        virtual bool getButton1State() = 0;
        virtual bool getButton2State() = 0;
        virtual bool getOpto01State() = 0;
        virtual bool getOpto02State() = 0;
        
        virtual bool syncPortStates() = 0;

    private:


};



#endif // _IODEVICE_H_