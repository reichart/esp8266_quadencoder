#ifndef _STM32_LIB_H_
#define _STM32_LIB_H_

#if ARDUINO < 100
  #include <WProgram.h>
#else
  #include <Arduino.h>
#endif

#include "iodevice.h"

#define STM32_SLAVE_ADDR            0x12

#define STM32_OBJ_PING              0x00

#define STM32_OBJ_INIT              0x10

#define STM32_OBJ_OUTPUT_ALL        0x16
#define STM32_OBJ_OUTPUT_BASE       0x17
#define STM32_OBJ_OUTPUT1   STM32_OBJ_OUTPUT_BASE
#define STM32_OBJ_OUTPUT2   STM32_OBJ_OUTPUT_BASE+1
#define STM32_OBJ_OUTPUT3   STM32_OBJ_OUTPUT_BASE+2
#define STM32_OBJ_OUTPUT4   STM32_OBJ_OUTPUT_BASE+3
#define STM32_OBJ_OUTPUT5   STM32_OBJ_OUTPUT_BASE+4

#define STM32_OBJ_PWM_BASE          0x20
#define STM32_OBJ_PWM_1      STM32_OBJ_PWM_BASE
#define STM32_OBJ_PWM_2      STM32_OBJ_PWM_BASE+1
#define STM32_OBJ_PWM_3      STM32_OBJ_PWM_BASE+2
#define STM32_OBJ_PWM_4      STM32_OBJ_PWM_BASE+3
#define STM32_OBJ_PWM_5      STM32_OBJ_PWM_BASE+4



#define STM32_OBJ_INPUTS_ALL        0x36
#define STM32_OBJ_INPUT_BASE        0x30
#define STM32_OBJ_INPUT1    STM32_OBJ_INPUT_BASE
#define STM32_OBJ_INPUT2    STM32_OBJ_INPUT_BASE+1
#define STM32_OBJ_INPUT3    STM32_OBJ_INPUT_BASE+2
#define STM32_OBJ_INPUT4    STM32_OBJ_INPUT_BASE+3
#define STM32_OBJ_INPUT5    STM32_OBJ_INPUT_BASE+4
#define STM32_OBJ_INPUT6    STM32_OBJ_INPUT_BASE+5


class STM32 : IODevice {
    public:
    
        void init(void);
        uint8_t getOutputs();
        uint8_t getInputs();

        bool getInput(uint8_t channel);
        bool setOutputs(uint8_t channels);
        bool setOutput(uint8_t channel, bool isHigh);
        bool toggleOutput (uint8_t channel);

        bool setPullups(uint8_t isUp);
        bool setPullup(uint8_t channel, bool isUp);

        bool getButton1State();
        bool getButton2State();
        bool getOpto01State();
        bool getOpto02State();
        
        bool syncPortStates();

        bool ping();

        /** 
         * @brief Set the pulsewidth for a channel (0-3)
         * @param channel        Channel that should be set (0-4)
         * @param pulseWidth     Pulse that should be set (0-100)
         */
        void setPwm(int channel, uint8_t pulseWidth);

    private:
        void writeRegister(uint8_t reg, uint8_t data);
        uint8_t readRegister(uint8_t reg);



};

#endif // _STM32_LIB_H_