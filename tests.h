#ifndef _TESTS_H_
#define _TESTS_H_

#if ARDUINO < 100
  #include <WProgram.h>
#else
  #include <Arduino.h>
#endif

#include "ExpanderMCP.h"

#define MCP_ADDRESS 0x20
#define BME_ADDRESS 0x76
#define SSD1306_ADDR      0x3C  // Display i2c-Address

class TestClass {

public:
  TestClass();
  void scani2c();
  void testMCP();
  void setPortA();
  void unsetPortA();
  bool isDisplayPresent();
  bool isBMEPresent();
  bool isMCPPresent();
  bool isSTMPresent();
  bool pingSTM32();

private:
  PortExpanderMCP mcp;
  bool foundMCP = false;
  bool foundDisplay = false;
  bool foundBME = false;
  bool foundSTM = false;
};

#endif // _TESTS_H_