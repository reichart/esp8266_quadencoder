#ifndef _PORTEXPANDER_MCP_H_
#define _PORTEXPANDER_MCP_H_

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#include "iodevice.h"

#define MCP23017_ADDRESS 0x20

// registers
#define MCP23017_IODIRA 0x00
#define MCP23017_IPOLA 0x02
#define MCP23017_GPINTENA 0x04
#define MCP23017_DEFVALA 0x06
#define MCP23017_INTCONA 0x08
#define MCP23017_IOCONA 0x0A
#define MCP23017_GPPUA 0x0C
#define MCP23017_INTFA 0x0E
#define MCP23017_INTCAPA 0x10
#define MCP23017_GPIOA 0x12
#define MCP23017_OLATA 0x14


#define MCP23017_IODIRB 0x01
#define MCP23017_IPOLB 0x03
#define MCP23017_GPINTENB 0x05
#define MCP23017_DEFVALB 0x07
#define MCP23017_INTCONB 0x09
#define MCP23017_IOCONB 0x0B
#define MCP23017_GPPUB 0x0D
#define MCP23017_INTFB 0x0F
#define MCP23017_INTCAPB 0x11
#define MCP23017_GPIOB 0x13
#define MCP23017_OLATB 0x15

#define MCP23017_INT_ERR 255


class PortExpanderMCP
{

public:
  enum MCP_PORT
  {
    MCP_PORT_A,
    MCP_PORT_B
  };

public:
  PortExpanderMCP(int address);

  bool setIODirection(MCP_PORT port, uint8_t direction);
  bool readPort(MCP_PORT port, uint8_t *data);
  bool writePort(MCP_PORT port, uint8_t data);

  bool setPullup(MCP_PORT port, uint8_t isUp);

  bool readRegister(uint8_t reg, uint8_t *data);
  bool writeRegister(uint8_t reg, uint8_t data);

private:
  uint8_t deviceAddress;

  bool writeByte(uint8_t data);
  bool readByte(uint8_t *data);
  bool readBytes(uint8_t length, uint8_t *data);
  uint8_t getPortSelectorValue(MCP_PORT port);

};

class BoardExpanderIO : IODevice{
    typedef struct PortState {
      uint8_t portConfig[2];
      uint8_t portPullUp[2];
      uint8_t portState[2];
    };

  private:
    PortExpanderMCP mcp;
    PortState portState;
    /** Read the hardware state of the board inputs */
    bool readAllInputs(void);
    /** Read the hardware state of the board outputs */
    bool readAllOutputs(void);

  public:
    BoardExpanderIO(int address);
    void init(void);
    /** Call readAllInputs() first to get the real hardware state  */
    bool getInput(uint8_t input);
    /** Call readAllInputs() first to get the real hardware state */
    uint8_t getInputs(void);
    bool setOutput(uint8_t input, bool isHigh);
    bool setOutputs(uint8_t isHigh);
    /** Call readAllOutputs() first to get the real hardware state */
    uint8_t getOutputs(void);
    bool toggleOutput(uint8_t pin);
    bool setPullups (uint8_t isUp);
    bool setPullup(uint8_t output, bool isUp);

    bool syncPortStates();

    bool getButton1State(void);
    bool getButton2State(void);
    bool getOpto01State(void);
    bool getOpto02State(void);
};

#endif // _PORTEXPANDER_H_
