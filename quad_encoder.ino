#include "iodevice.h"
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include "ExpanderMCP.h"
#include "tests.h"

#define DEBUG

BoardExpanderIO mcp(0x20);
TestClass test;

bool encIn1;
bool encIn2;
bool isMoveUpwards;
int32_t encoderCount;

bool lastEncoderDirection;
int32_t lastEncoderCount;
uint32_t pulseCount;

void handleEncoderInput(bool in1, bool in2);

void setup() {
  Wire.begin(0x10);
  Wire.setClock(1000000);

  Serial.begin(230400);

  Serial.println("Starting");

  test.scani2c();
  if(test.isMCPPresent()){
    Serial.println("MCP detected");
    test.testMCP();
    mcp.init();
  }

  encIn1=false;
  encIn2=false;
  encoderCount=0;
  isMoveUpwards = true;
  pulseCount=0;

/*
  for(uint8_t i=0;i<8;++i){
    Serial.print("Setting Output ");
    Serial.println(i);
    mcp.setOutput(i, true);
    for(uint8_t j=0;j<10;++j){
      yield();
      delay(100);
    }
    mcp.setOutput(i, false);
  }
  */
}

void loop() {
  mcp.syncPortStates();
  bool tmpIn1 = mcp.getOpto01State();
  bool tmpIn2 = mcp.getOpto02State();
  handleEncoderInput(tmpIn1, tmpIn2);

  if (lastEncoderCount!=encoderCount){
    isMoveUpwards = (lastEncoderCount<encoderCount);
    lastEncoderCount = encoderCount;
    
    #ifdef DEBUG
    Serial.print("Current EncoderValue <");
    Serial.print(encoderCount);
    Serial.print("> DirectionUpwards<");
    Serial.print( (isMoveUpwards)?"True":"False");
    Serial.print(">\n");
    #endif // DEBUG
  }

  if (lastEncoderDirection!=isMoveUpwards){
    if(lastEncoderDirection){
      pulseCount=1000;
      mcp.setOutput(4, true);
      mcp.setOutput(5, true);
    } else {
      mcp.setOutput(5, false);
    }
    lastEncoderDirection=isMoveUpwards;
  }

  if(pulseCount!=0){
    pulseCount--;
  } else {
    mcp.setOutput(4, false);
  }
}

void handleEncoderInput(bool in1, bool in2){
  if(in1==encIn1 && in2==encIn2) return;

#ifdef DEBUG
  Serial.print("Input01<");
  Serial.print(in1);
  Serial.print("> Input02<");
  Serial.print(in2);
  Serial.print(">\n");
#endif // DEBUG

  if(in1!=encIn1){
    // Input1 has changed
    if(in1){
      (in2) ? encoderCount++:encoderCount--;
    } else {
      (in2) ? encoderCount--:encoderCount++;
    }
  } else if(in2!=encIn2){
    // Input2 has changed
    if(in2){
      (in1) ? encoderCount--:encoderCount++;
    } else {
      (in1) ? encoderCount++:encoderCount--;
    }
  } else {
    Serial.println("Unexpected Input-State");
  }

  encIn1 = in1;
  encIn2 = in2;
}
