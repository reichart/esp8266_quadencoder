#include "Wire.h"
#include "stm32lib.h"

void STM32::init(void){
    writeRegister(STM32_OBJ_INIT, 0x01);  // Register 0x10 -> Set first bit for slave function
}

uint8_t STM32::getOutputs(){
    return readRegister(STM32_OBJ_OUTPUT_ALL);
}

uint8_t STM32::getInputs(){
    uint8_t result = readRegister(STM32_OBJ_INPUTS_ALL);
}

bool STM32::getInput(uint8_t channel){
    uint8_t result = readRegister(STM32_OBJ_INPUT_BASE+channel);
}

bool STM32::setOutputs(uint8_t channels){
    writeRegister(STM32_OBJ_OUTPUT_ALL, channels);

}

bool STM32::setOutput(uint8_t channel, bool isHigh){
    writeRegister(STM32_OBJ_OUTPUT_BASE + channel, isHigh);
}

bool STM32::toggleOutput(uint8_t channel){
    uint8_t currentState = readRegister(STM32_OBJ_OUTPUT_BASE+channel);
    writeRegister(STM32_OBJ_OUTPUT_BASE+channel, !currentState);
}

bool STM32::getButton1State() {

}

bool STM32::getButton2State(){

}

bool STM32::getOpto01State(){

}

bool STM32::getOpto02State(){

}

bool STM32::syncPortStates(){

}

bool STM32::ping(){
    uint8_t testValue = 0xE1;
    uint8_t result = readRegister(STM32_OBJ_PING);
    Serial.printf("STM32-Ping: Sent %x and received %x\n", testValue, result);
    return (0xE8 == result); // A read request sends a 0xE7 as 2nd byte and for PONG a 0xE8 is returned
}

void STM32::setPwm(int channel, uint8_t pulseWidth) {
    if (channel>3)return;
    writeRegister(STM32_OBJ_PWM_BASE+channel, pulseWidth); // Register 0x20(+4) -> PWM Output
}

void STM32::writeRegister(uint8_t reg, uint8_t data) {
    Wire.beginTransmission(STM32_SLAVE_ADDR);
    Wire.write(reg);
    Wire.write(data);
    Wire.endTransmission();
}

uint8_t STM32::readRegister(uint8_t reg){
    Wire.beginTransmission(STM32_SLAVE_ADDR);
    Wire.write(reg|0x80);   // Set the MVB -> read operation
    Wire.write(0xE7);       // read operation
    Wire.endTransmission();
    Wire.requestFrom(STM32_SLAVE_ADDR, 1);  // Read 1 byte
    int result;

    while(Wire.available()){
        result = Wire.read();
        Serial.print("I2C Data:");
        Serial.print(result);
        Serial.print("\n");
    }
    return result;
}

