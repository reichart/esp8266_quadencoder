#ifndef _PORTEXPANDER_MCP_CPP_
#define _PORTEXPANDER_MCP_CPP_

#include "ExpanderMCP.h"
#include <Wire.h>

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

BoardExpanderIO::BoardExpanderIO(int address) :
  mcp(address) {
    Serial.println("BoardExpanderIO: running the constructor");
    //Initialize the inputs and outputs for the BoardExpanderIO
    mcp.setIODirection(PortExpanderMCP::MCP_PORT_A, 0x00); // Outputs
    mcp.setIODirection(PortExpanderMCP::MCP_PORT_B, 0xFF); // Inputs
    setOutputs(0x00); // All outputs LO
}

void BoardExpanderIO::init(){
      //Initialize the inputs and outputs for the BoardExpanderIO
    mcp.setIODirection(PortExpanderMCP::MCP_PORT_A, 0x00); // Outputs
    mcp.setIODirection(PortExpanderMCP::MCP_PORT_B, 0xFF); // Inputs
    setPullups(0x1F);
    // setPullup(0, true);
    // setPullup(1, true);
    // setPullup(2, true);
    // setPullup(3, true);
    // setPullup(4, true);
    setOutputs(0x00); // All outputs LO
}

bool BoardExpanderIO::setOutput(uint8_t address, bool isHi){
  #ifdef TRACE_VERSION
  Serial.print("SetOutput() Was<0x");
  Serial.print(String(portState.portState[0], 16));
  #endif //TRACE_VERSION
  if (isHi){
    portState.portState[0] |= (1<<address);
  } else {
    portState.portState[0] &= ~(1<<address);
  }
  #ifdef TRACE_VERSION
  Serial.print("> Change to<0x");
  Serial.print(String(portState.portState[0], 16));
  Serial.print(">\n");
  #endif // TRACE_VERSION

  return mcp.writePort(PortExpanderMCP::MCP_PORT_A, portState.portState[0]);
}

bool BoardExpanderIO::setOutputs(uint8_t isHi){
  portState.portState[0]=isHi;
  return mcp.writePort(PortExpanderMCP::MCP_PORT_A, portState.portState[0]);
}

bool BoardExpanderIO::toggleOutput(uint8_t pin){
  syncPortStates();
  boolean isHi = (portState.portState[0] & (1<<pin));
  if (isHi){
    setOutput(pin, false);
  } else {
    setOutput(pin, true);
  }
}

bool BoardExpanderIO::readAllInputs(void){
  return mcp.readPort(PortExpanderMCP::MCP_PORT::MCP_PORT_B, &portState.portState[1]);
}

bool BoardExpanderIO::readAllOutputs(void){
  return mcp.readPort(PortExpanderMCP::MCP_PORT::MCP_PORT_A, &portState.portState[0]);
}

bool BoardExpanderIO::getInput(uint8_t input){
  return portState.portState[1] & (1<<input);
}

uint8_t BoardExpanderIO::getInputs(void){
  return portState.portState[1];
}

uint8_t BoardExpanderIO::getOutputs(void){
  return portState.portState[0];
}

bool BoardExpanderIO::getButton1State(void){
  Serial.println("Button 1 State is not available in this configuration");
  return false;
}
bool BoardExpanderIO::getButton2State(void){
  return getInput(0x07);
}

bool BoardExpanderIO::getOpto01State(void){
  return getInput(0x05);
}

bool BoardExpanderIO::getOpto02State(void){
  return getInput(0x06);
}

bool BoardExpanderIO::setPullups(uint8_t isUp){
  portState.portPullUp[0] = isUp;
  return mcp.setPullup(PortExpanderMCP::MCP_PORT::MCP_PORT_B, portState.portPullUp[0]);
}

bool BoardExpanderIO::setPullup(uint8_t output, bool isHi){
  if (isHi){
    portState.portPullUp[0] |= (1<<output);
  } else {
    portState.portPullUp[0] &= ~(1<<output);
  }
  return mcp.setPullup(PortExpanderMCP::MCP_PORT::MCP_PORT_B, portState.portPullUp[0]);
}

bool BoardExpanderIO::syncPortStates(){
  bool result = readAllOutputs();
  result |= readAllInputs();
  return result;
}


/* ------------------------------------------------------------------------------- */

PortExpanderMCP::PortExpanderMCP(int address) :
deviceAddress(address)
{
  Wire.begin();
};

bool PortExpanderMCP::readRegister(uint8_t reg, uint8_t *data){
  writeByte(reg);
  Wire.requestFrom(deviceAddress, 1, true);
    if (Wire.available()){
      *data = Wire.read();
    } else return false;
  return true;
}

bool PortExpanderMCP::writeRegister(uint8_t reg, uint8_t data){
  Wire.beginTransmission(deviceAddress);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t) data);
  Wire.endTransmission();
  return true;
}

bool PortExpanderMCP::readPort(MCP_PORT port, uint8_t *data){
  return readRegister(getPortSelectorValue(port), data);
}

bool PortExpanderMCP::writePort(MCP_PORT port, uint8_t data){
  return writeRegister(getPortSelectorValue(port), data);
}

bool PortExpanderMCP::setIODirection(MCP_PORT port, uint8_t direction){
  Wire.beginTransmission(deviceAddress);
  uint8_t portRegister;
  if (port == PortExpanderMCP::MCP_PORT_A) {
    portRegister = MCP23017_IODIRA;
  } else {
    portRegister = MCP23017_IODIRB;
  }
  Wire.write(portRegister);
  Wire.write(direction);
  Wire.endTransmission();
}

bool PortExpanderMCP::setPullup(MCP_PORT port, uint8_t isUp){
  Wire.beginTransmission(deviceAddress);
  uint8_t portRegister = (PortExpanderMCP::MCP_PORT::MCP_PORT_A == port) ? MCP23017_GPPUA : MCP23017_GPPUB;
  Serial.print("PORTEXPANDER: Writing<");
  Serial.print(String(isUp,16));
  Serial.print("> to Register <");
  Serial.print(String(portRegister,16));
  Serial.print(">\n");
  Wire.write(portRegister);
  Wire.write(isUp);
  Wire.endTransmission();
}

bool PortExpanderMCP::writeByte(uint8_t data){
  Wire.beginTransmission(deviceAddress);
  Wire.write(data);
  Wire.endTransmission();
}

/** Only functional when using IOCON Bank=1 
 * Do not use
*/
uint8_t PortExpanderMCP::getPortSelectorValue(MCP_PORT port){
  uint8_t portSelector = 0x00;
  if (port == PortExpanderMCP::MCP_PORT::MCP_PORT_A){
    return MCP23017_GPIOA;
  } else {
    return MCP23017_GPIOB;
  }
 }

  #endif // _PORTEXPANDER_MCP_CPP_